package aed;

import java.io.PrintStream;

class Intercalar {
    void imprimirStrings(String intercalacion, String primero, String segundo, PrintStream salida) {
        /* Intercala e imprime dos strings dados
         * Si la entrada es primero = "ab" y segundo = "cd", sobre la salida se debe imprimir:
         * abcd acbd acdb cabd cadb cdab 
         */
        // Caso base
        boolean estanVacios = primero.length() == 0 && segundo.length() == 0;
        if (estanVacios) {
            salida.print(intercalacion + " ");
            return;
        }
        // Quito el primer caracter y hago recursion sobre la cola del primero
        char primeroPrimero = primero.charAt(0);
        imprimirStrings(intercalacion + primeroPrimero, cola(primero), segundo, salida);
        // Quito el primer caracter y hago recursion sobre la cola del segundo
        char primeroSegundo = segundo.charAt(0);
        imprimirStrings(intercalacion + primeroSegundo, primero, cola(segundo), salida);
    }

    String cola(String s){
        String res = "";
        for (int i = 1; i < s.length(); i++) {
            res += s.charAt(i);
        }
        return res;
    }
}
